// SIMULATED DATAS SENT BY ENGINE
menuList = [["Back","Back to the game","MXC"],["Settings","Settinds","Mset", true],["Upgrades","Upgrade your weapon","Mup", true],["Map","Map","Mmap", true],["Achievements","Achievements list","Machies", true],["Exit"]];
settingList = ["general settings","slider",["Minimum","Medium","Maximum","Ultra"]];


/**
 * Menu module
 * Code definatelly could be more structurized, but it takes into account original sources and data structures.
 * I modified some, but tried to maintain some consistency with original script.
 */
const PauseMenu = (function() {
    const $menu = $1('menu');
    const $clic = $1('clic');
    const $content = $1('content');
    const $contentHeader = $1('contentHeader');
    const $contentBody = $1('contentBody');
    let menu;
    let setting;


    function clic(){
        $clic.play();
    }
    function clicOff(){
        $clic.pause();
        $clic.currentTime = 0;
    }

    function renderSlider(cfg){
        const labels = document.createElement('ul');
        const input = document.createElement('input');
        const slider = document.createElement('section');
        const optionsCount = cfg[2].length -1;
        let i;

        input.setAttribute('type', 'range');
        input.setAttribute('id', cfg[0]);
        input.setAttribute('name', cfg[0]);
        input.setAttribute('min', 0);
        input.setAttribute('max', cfg[2].length - 1);

        i = 0;
        while(i <= optionsCount){
            let li = document.createElement('li');
            li.textContent = cfg[2][i]
            labels.appendChild(li);
            i++
        }

        slider.setAttribute('id', cfg[0] + 'Container');
        slider.classList.add(cfg[0].replace(' ', '-') + '-container');
        slider.classList.add('slider');
        slider.appendChild(input);
        slider.appendChild(labels);

        return slider;
    }

    function renderSettings(){
        const header = document.createElement('h3');
        header.textContent = setting[0];

        $contentBody.innerHTML = '';
        $contentBody.appendChild(header);
        if (setting[1] == 'slider'){
            $contentBody.appendChild(renderSlider(setting));
        }

    }

    function renderEmpty(){
        $contentBody.innerHTML = '';
    }

    function showContent(el){
        $content.classList.add('visible');
        $contentHeader.textContent = menu[el][1];
        if (menu[el][2] == 'Mset'){
            renderSettings();
        } else {
            renderEmpty();
        }
    }

    function hideContent(){
        $content.classList.remove('visible');
    }

    function initContent(){
        $1('back').addEventListener('click', hideContent);
        $1('apply').addEventListener('click', hideContent);
    }

    function initMenu(){
        const lines = menu.length;
        let i = 0;
        while (i < lines){
            const item = document.createElement('li');
            const final = "<div'>"+menu[i][0]+"</div>";
            item.setAttribute("id",menu[i][2]);
            item.style.animationDelay =  (i * 150) + 'ms';
            item.innerHTML = final;
            item.addEventListener('mouseenter', clic);
            item.addEventListener('mouseleave', clicOff);
            if (menu[i][3]){
                item.addEventListener('click', showContent.bind(item, i));
            }
            $menu.appendChild(item);
            i++;
        }
    }

    function init(menuList){
        menu = menuList;
        setting = settingList;
        initMenu();
        initContent(); 
    }

    return {
        init
    }

})();

PauseMenu.init(menuList, settingList);

function $(selector, context) {return (context || document).querySelectorAll(selector);}
function $1(id) {return document.getElementById(id)}