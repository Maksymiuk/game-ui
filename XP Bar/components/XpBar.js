/**
 * XP progress bar component
 * @author Rafał Maksymiuk <rafal.maksymiuk@gmail.com>
 * @created 02.04.2019
 */

class XpBar extends HTMLElement {
    constructor() {
        super();
 
        this.xp = 0;
        this.barSize = 1000;
        this.barPosition = 0;
        this.animating = false;

        const shadowRoot= this.attachShadow({mode: 'open'});
 
        const styles=`
            :host {
                position: relative;
                font-family: sans-serif;
            }
 
            .container {
                position: relative
            }
            .bar {
                height: 30px;
                margin-right: 200px;
                box-sizing: border-box;
                overflow: hidden;
                position: relative;

                border: 2px solid lightblue;
                background-color: lightblue;
            }
            .bar .bar-bg {
                height: 100%;
                width: 100%;
                
                background-color: white;
                transition: transform 1s cubic-bezier(0.22, 0.61, 0.36, 1);
                transform: translateX(0);
            }
            .bar .bar-bg.reset {
                transition: none !important;
                transform: translateX(0);
            }


            @keyframes levelUp {
                0% {
                    opacity: 0;
                    transform: rotate(-10deg) scale(0.25);
                    animation-timing-function: ease-in;
                }
                28%, 80% {
                    opacity: 1;
                    transform: rotate(-10deg) scale(1);
                    animation-timing-function: linear;
                }
                100% {
                    opacity: 0;
                    transform: rotate(-10deg) scale(0.5);
                    animation-timing-function: ease-out;
                }
            }

            .level-up{
                position: absolute;
                right: 240px;
                top: -15px;

                font-size: 3em;
                opacity: 0;
            }
            .level-up.animate{      
                animation-name: levelUp;
                animation-duration:5s;
                animation-iteration-count: 1;
                animation-timing-function: ease;
            }

            button {
                width: 80px;
                height: 30px;
                box-sizing: border-box;
                position: relative;

                border: 2px solid lightblue;
                border-radius: 9px;
                background: white;
                outline: none;
   
                cursor: pointer;
            }
            button::after{
                content: "";
                width: 100%;
                height: 100%;
                position: absolute;
                top: 0;
                left: 0;

                background-color: lightblue;
                opacity:0;
            }
            @keyframes cssAnimation {
                0% {
                    opacity:0.01;
                }
                50% {
                    opacity:0.5;
                }
                100% {
                    opacity:0.01;
                }
            }
            button.active::after{      
                animation-name: cssAnimation;
                animation-duration:0.2s;
                animation-iteration-count: 1;
                animation-timing-function: ease;
            }

            .add {
                position: absolute;
                top: 0;
                right: 100px;
            }
            .reset {
                position: absolute;
                top: 0;
                right: 10px;
            }

        `;
 
        const html = `
            <style>${styles}</style>
            <section class="container">
                <div id="bar" class="bar">
                    <div id="barBG" class="bar-bg"></div>
                </div>
                <div id="levelUp" class="level-up">Level UP!</div>
                <button id="add" class="add"> Add 1200</button>
                <button id="reset" class="reset"> Reset </button>
                <audio id="audio" preload="auto" src="components/UI_levelup_N.wav">
                    Your browser does not support the <code>audio</code> element.
                </audio>
            </section>
        `;

        shadowRoot.innerHTML = html;
            
        this.bar = this.shadowRoot.getElementById('barBG');
        this.levelUp = this.shadowRoot.getElementById('levelUp');
        this.audio = this.shadowRoot.getElementById('audio');
        this.shadowRoot.getElementById('add').addEventListener('click', this.clickAnimation.bind(this));
        this.shadowRoot.getElementById('add').addEventListener('click', this.addPoints.bind(this, 1200));
        this.shadowRoot.getElementById('reset').addEventListener('click', this.clickAnimation.bind(this));
        this.shadowRoot.getElementById('reset').addEventListener('click', this.resetBar.bind(this, true));
    }
 
    /**
     * Animate click button 
     * @param {Event} e 
     */
    clickAnimation(e){
        if (this.animating){
            return;
        }
        let element = e.currentTarget;
        element.classList.add('active');
        setTimeout(() => element.classList.remove('active'), 200);
    }

    /**
     * Set bar position to given number (in game measure)
     * @param {Number} position in xp points relative to begin
     */
    setBarPosition(position){
        let fill = (position / this.barSize) * 100;
        let length = Math.ceil(((position - this.barPosition) / this.barSize) * 2000);
        this.bar.style.transform = `translateX(${fill}%)`; 
        this.bar.style.transitionDuration = `${length}ms`; 
        this.barPosition = position;
        return length;
    }

    /**
     * Render full bar
     */
    fullBar(){
        return new Promise (resolve => {
            let time = this.setBarPosition(this.barSize);
            time = time > 500 ? time - 500 : 0;
            setTimeout( () => {
                resolve();
            }, time);
        });
    }

    /**
     * Reset bar to 0
     * @param {Boolean} data also reset data
     */
    resetBar(data = false){
        this.bar.classList.add('reset');
        this.bar.style.transform = `translateX(0)`; 
        this.bar.offsetHeight;
        this.bar.classList.remove('reset');
        this.barPosition = 0;
        if (data){
            this.xp = 0;
        }
    }

    /**
     * Play next level animation
     */
    nextLevel(){
        return new Promise (resolve => {
            this.levelUp.classList.add('animate');
            this.audio.play();

            setTimeout( () => {
                resolve();
            }, 4000);
            setTimeout( () => {
                this.levelUp.classList.remove('animate');
            }, 5000);
        });
    }

    /**
     * Add XP to progress bar
     * @param {Number} points 
     */
    async addPoints(points){
        if (this.animating){
            return;
        }

        let pointsToDraw = points;
        this.animating = true;

        while (pointsToDraw + this.barPosition >= this.barSize) {
            pointsToDraw -= (this.barSize - this.barPosition);
            await this.fullBar();
            await this.nextLevel();
            this.resetBar();
        }

        this.setBarPosition(pointsToDraw + this.barPosition);
        this.xp += points;
        this.animating = false;
    }

}

customElements.define('xp-bar', XpBar);